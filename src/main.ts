import { NestFactory } from '@nestjs/core';
import { RadarModule } from './radar/radar.module';
import { ValidationPipe } from '@nestjs/common';


async function bootstrap() {
  const app = await NestFactory.create(RadarModule);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(8888);
}
bootstrap();
