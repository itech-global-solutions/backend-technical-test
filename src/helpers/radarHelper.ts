import { EnemyEnum } from './../radar/enums/EnemyEnum';
import { ScanDto } from '../radar/dto/radar.dto';

const calculateDistace = (coordinates) =>
  Math.sqrt(Math.pow(coordinates.x, 2) + Math.pow(coordinates.y, 2));

export const addDistance = (Radar: ScanDto[]): ScanDto[] => {
  return Radar.map((point) => ({
    ...point,
    distance: calculateDistace(point.coordinates),
  }));
};

export const discardUnreachableEnemies = (
  Radar: ScanDto[],
  distance: number = 100,
): ScanDto[] => {
  return Radar.filter((point) => point.distance < distance);
};


export const sortMech = (a, b) => {
  if (
    a.enemies.type === EnemyEnum.MECH &&
    b.enemies.type !== EnemyEnum.MECH
  ) {
    return -1;
  } else if (
    a.enemies.type !== EnemyEnum.MECH &&
    b.enemies.type === EnemyEnum.MECH
  ) {
    return 1;
  } else {
    return 0;
  }
};


export const sortAllies = (a, b) => {
  if (a.allies && !b.allies) {
    return -1;
  } else if (!a.allies && b.allies) {
    return 1;
  } else {
    return 0;
  }
}