import { Radar } from './entities/radar.entity';
import { CreateRadarDto } from './../../dist/radar/dto/create-radar.dto.d';
import { Injectable } from '@nestjs/common';
import { RadarDto, ScanDto } from './dto/radar.dto';
import { EnemyEnum } from './enums/EnemyEnum';
import {
  addDistance,
  discardUnreachableEnemies,
  sortAllies,
  sortMech,
} from '../helpers/radarHelper';

@Injectable()
export class RadarService {

  ProtocolsMapper: object = Object.freeze({
    'furthest-enemies': this.furthestEnemies,
    'avoid-mech': this.avoidMechs,
    'avoid-crossfire': this.avoidCrossfire,
    'prioritize-mech': this.prioritizeMechs,
    'assist-allies': this.assistAllies,
    'closest-enemies': this.closestEnemies,
  });

  checkObjectives(RadarDto: RadarDto) {
    const { protocols, scan } = RadarDto;

    let scanTmp: ScanDto[] = [...scan];

    scanTmp = addDistance(scanTmp);
    scanTmp = discardUnreachableEnemies(scanTmp);

    protocols.forEach((protocol) => {
      scanTmp = this.ProtocolsMapper[protocol](scanTmp);
      return scanTmp;
    });
    return scanTmp[0].coordinates;
  }

  furthestEnemies(Scan: ScanDto[]): ScanDto[] {
    return Scan.sort((a, b) => b.distance - a.distance);
  }
  closestEnemies(Scan: ScanDto[]): ScanDto[] {
    return Scan.sort((a, b) => a.distance - b.distance);
  }
  avoidMechs(Scan: ScanDto[]): ScanDto[] {
    return Scan.filter((point) => point.enemies.type !== EnemyEnum.MECH);
  }
  avoidCrossfire(Scan: ScanDto[]): ScanDto[] {
    return Scan.filter((point) => !point.allies);
  }
  prioritizeMechs(Scan: ScanDto[]): ScanDto[] {
    return Scan.sort(sortMech);
  }
  assistAllies(Scan: ScanDto[]): ScanDto[] {
    return Scan.sort(sortAllies);
  }
}
