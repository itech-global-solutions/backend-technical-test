import { Controller, Post, Body } from '@nestjs/common';
import { RadarService } from './radar.service';
import { RadarDto } from './dto/radar.dto';

@Controller('radar')
export class RadarController {
  constructor(private readonly radarService: RadarService) {}

  @Post()
  
  checkObjectives(@Body() RadarDto: RadarDto) {
    return this.radarService.checkObjectives(RadarDto);
  }
}
