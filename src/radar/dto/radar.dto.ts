import {IsNotEmpty} from 'class-validator';

export class RadarDto {

  @IsNotEmpty()
  protocols: string[];
  
  @IsNotEmpty()
  scan: ScanDto[];
}

export class ScanDto {
  coordinates: {
    x: number;
    y: number;
  };
  enemies: {
    type: string;
    number: number;
  };
  allies?: number;
  distance?: number;
}
